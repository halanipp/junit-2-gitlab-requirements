#!/usr/bin/env node

// Parse inputs
function readJsonFile(relativePath) {
  const absolutePath = require("path").resolve(process.cwd(), relativePath);
  const jsonContent = require("fs").readFileSync(absolutePath, "utf-8");
  return JSON.parse(jsonContent);
}

const reqIds = readJsonFile(process.argv[2]),
  tests = readJsonFile(process.argv[3]);

// Perform mapping
const REQ_FAILED = "failed",
  REQ_PASSED = "passed",
  TEST_SUCCESS = "success";

const statuses = Object.fromEntries(
  Object.entries(tests)
    .filter(([name]) => name in reqIds)
    .map(([name, status]) => [reqIds[name], status == TEST_SUCCESS ? REQ_PASSED : REQ_FAILED])
);

console.log(JSON.stringify(statuses));
